from dataclasses import dataclass, asdict
from datetime import datetime, timedelta

from bson.objectid import ObjectId
from pymongo.collection import Collection

from infra import db
from utils import now, dataclass_from_dict


@dataclass
class Onboarding:
    started: datetime = now()
    expires: datetime = timedelta(days=1) + now()
    finished: datetime = None
class EntityException(Exception):
    pass
class EntityNotFoundException(EntityException):
    pass
class EntityNotDeletedException(EntityException):
    pass

@dataclass
class User:
    email: str
    given_name: str= None
    family_name: str = None
    phone: str = None
    """ Optional, entspricht dem sub-Claim aus dem oidc-access-token"""
    sub: str = None

    onboarding: Onboarding = None

    def __post_init__(self):
        if isinstance(self.onboarding, dict):
            self.onboarding = Onboarding(**self.onboarding)

    _id: ObjectId = None
    created: datetime = now()

    ### repository ...
    ## TODO: diese methoden sollte generisch sein .. db.<collection> abhaengig von der cls

    def as_dict(self):
        return { k:v for k,v in asdict(self).items() if v }
    def id(self)->str:
        if self._id: return str(self._id)
        else: return None

    @classmethod
    def from_dict(cls, data: dict):

        res=dataclass_from_dict(cls, data)
        if isinstance(res,cls):
            return res
        raise Exception(f"Unable to build a {cls }from dict:{data}")

    @classmethod
    def by_id(cls,id:str):
        ## TODO: diese methode sollte generisch sein .. db.<collection> abhaengig von der cls
        entity=cls.collection().find_one({"_id": ObjectId(id)})
        if not entity: raise EntityNotFoundException(cls,id)
        return dataclass_from_dict(cls, entity)

    @classmethod
    def collection(cls)->Collection:
        return db.users

    def save(self):
        if self._id is None:
            # data=self.as_dict()
            # data.pop('_id')
            res=self.collection().insert_one(self.as_dict())
            self._id=res.inserted_id
        else:
            ## TODO: result pruefen??
            res = self.collection().update_one(self._filter(), {'$set': self.as_dict()})
        return self

    def delete(self):
        if self._id is not None:
            res=self.collection().delete_one(self._filter())
            if res.deleted_count != 1: raise EntityNotDeletedException(self.__class__, self._id)
            self._id=None

    def _filter(self):
        return {"_id": self._id}


def setup_db():
    User.collection().create_index("email", unique=True)
    User.collection().create_index("sub", unique=True, sparse=True)


__all__ = ["User","Onboarding", "EntityNotFoundException"]