## Lokale Entwicklung

api: http://localhost:5000/

### Backend Dienste

#### MongoDB

mongo-db: mongodb://root:example@localhost:27017/

mongo-express: http://localhost:8081/

#### mailhog

smtp: smtpt://localhost:1025

frontend: http://localhost:8025

### jwt...

das Schlüsselpaar in keys, wird erzeugt mit

```commandline
ssh-keygen -t rsa -f keys/id_rsa -N ''    
```

