import importlib.resources as pkg_resources

from cryptography.hazmat.primitives import serialization

with pkg_resources.open_binary(__name__, 'id_rsa') as file:
    private_key = serialization.load_ssh_private_key(file.read(), password=b'')

with pkg_resources.open_binary(__name__, 'id_rsa.pub') as file:
    public_key = serialization.load_ssh_public_key(file.read())
