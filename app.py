from flask import Flask,request
from flask_restful import Resource, Api


app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

api.add_resource(HelloWorld, '/')

users={}

class User(Resource):
    def get(self,id):
        return { "user": users[id]}


class UserList(Resource):
    def get(self):
        return users
    def post(self):
        pass


api.add_resource(UserList, '/users')
api.add_resource(User, '/users/self','/users/<string:id>')



vereine={}
class VereinResource(Resource):
    def get(self,id):
        return vereine[id]
    def patch(self,id):
        pass
    def post(self,id):
        request.get_json()




if __name__ == '__main__':
    app.run(debug=True)