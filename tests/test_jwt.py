import datetime

import jwt
from cryptography.hazmat.primitives import serialization


def encode_auth_token(self, user_id):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=5),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            app.config.get('SECRET_KEY'),
            algorithm='HS256'
        )
    except Exception as e:
        return e


payload_data = {
    "sub": "7c3b4454-c767-4fe7-b4e1-93505f676968",
    "email": "mha-root@localhost.de",
    "aud": "http://billy.de"
}
my_secret = 'my_super_secret'

token = jwt.encode(
    payload=payload_data,
    key=my_secret
)
validated_sym = jwt.decode(token, key=my_secret, algorithms=['HS256', ], audience=payload_data['aud'])

private_key_data = open('keys/id_rsa', 'r').read()
private_key = serialization.load_ssh_private_key(private_key_data.encode(), password=b'')
public_key_data = open('keys/id_rsa.pub', 'r').read()
public_key = serialization.load_ssh_public_key(public_key_data.encode())

new_token = jwt.encode(
    payload=payload_data,
    key=private_key,
    algorithm='RS256'
)

# read and load the key

validated_asym = jwt.decode(jwt=new_token, key=public_key, algorithms=['RS256', ], audience=payload_data['aud'])
