import json
import uuid

from utils import JSONEncoder, now


def test_dump_uuid():
    src={'id': uuid.uuid4()}
    src_json=json.dumps(src,cls=JSONEncoder)

    target=json.loads(src_json);
    assert str(src['id']) == target['id']


def test_dump_datetime():
    src={'now': now()}
    src_json=json.dumps(src,cls=JSONEncoder)

    target=json.loads(src_json);
    assert src['now'].isoformat() == target['now']
