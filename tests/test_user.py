import random
import string

import pytest
import requests


@pytest.fixture
def api():
    return "http://localhost:5000/users"
def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))

def test_cru_user(api):
    user_nr = id_generator()
    initial_data = {'email': f"junit+{user_nr}@nowhere.com", 'family_name': f'Meier {user_nr}',
                    'given_name': f'Hans {user_nr}'}
    # create

    r = requests.post(api, json=initial_data)
    assert r.status_code == 201
    user = r.json()
    user_id = user["_id"]
    # read
    r = requests.get(api + "/" + user_id)
    assert r.status_code == 200
    user=r.json()
    for k,v in initial_data.items():
        assert user[k] == v

    update_data = {'phone': '123', 'given_name': f'Meier-Maier {user_nr}'}
    # update
    r = requests.patch(api + "/" + user_id, json=update_data)
    assert r.status_code == 200

    assert r.json()['email'] == initial_data['email']
    assert r.json()['given_name'] == update_data['given_name']
    assert r.json()['phone'] == update_data['phone']


def test_user_onboarding(api):
    user_nr = id_generator()
    initial_data = {'email': f"junit+{user_nr}@nowhere.com"}
    r = requests.post(api, json=initial_data)
    assert r.status_code == 201
    user = r.json()
    user_id = user["_id"]

    ## read ..   : datetime = now()
    r = requests.get(api + "/" + user_id + "/onboarding")
    assert r.json()['started'] is not None
    assert r.json()['expires'] is not None
    assert r.json().get('finished') is None

    ## finish
    r = requests.delete(api + "/" + user_id + "/onboarding")
    assert r.status_code == 204

    r = requests.get(api + "/" + user_id + "/onboarding")
    assert r.json()['started'] is not None
    assert r.json()['expires'] is not None
    assert r.json()['finished'] is not None

    ## second time, its gone
    r = requests.delete(api + "/" + user_id + "/onboarding")
    assert r.status_code == 404
