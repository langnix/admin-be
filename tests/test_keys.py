import keys


def test_private_key():
    assert keys.private_key is not None


def test_public_key():
    assert keys.public_key is not None
