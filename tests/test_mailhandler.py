from entities import User, Onboarding
from services import mailhandler


def test_send_onboarding():
    user = User("dummy@nowhere.com")
    user.onboarding = Onboarding()

    mailhandler.send_onboarding(user)
