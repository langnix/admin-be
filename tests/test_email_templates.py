from email_templates import _load_templates, _get_mail_definition, gen_mail_message


def test_load_templates():
    _load_templates()


def test_get_mail_definition():
    mt = _get_mail_definition('onboarding')
    assert mt.subject is not None


def test_gen_mail_message():
    data = {'welcome_url': "http://som.de/#some?para=77", 'expires': 'bald..'}

    msg = gen_mail_message('onboarding', welcome_url="http://som.de/#some?para=77", expires='bald..')
    assert msg.get('Subject') is not None
    assert msg.get('From') is not None
    assert "bald.." in msg.get_content()
    msg = gen_mail_message('onboarding-done', profile_url='http://som.de/#profile')


def test_gen_mail_with_missing_template_param():
    msg = gen_mail_message('onboarding', welcome_url="http://som.de/#some?para=77")
    assert "http://som.de/#some?para=77" in msg.get_content()
    assert msg.get('Subject') is not None
    assert msg.get('From') is not None
