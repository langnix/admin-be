import pytest
from bson import ObjectId

from entities import User,Onboarding,EntityNotFoundException


def test_crud_user():
    cut=User("otto")
    assert cut.id() is None
    cut.save()
    assert cut.id() is not None

    c2=User.by_id(cut.id())
    assert cut == c2

    cut.onboarding=Onboarding()
    cut.save()

    c2 = User.by_id(cut.id())
    assert cut == c2

    used_id=cut.id()
    cut.delete()

    with pytest.raises(EntityNotFoundException):
        User.by_id(used_id)


def test_from_dict():
    cut = User("otto")
    cut.save()
    cut_d= cut.as_dict()
    assert cut_d['_id'] is not None
    c2=User.from_dict(cut_d)

    assert cut == c2
    cut.delete()



def test_full_user_from_dict():
    u0 = User('hansi@nowhere.com')
    u0._id = ObjectId()
    u0.given_name = 'Hansi'
    u0.family_name = 'Meiser'
    u0.onboarding = Onboarding()

    user_dict = u0.as_dict()

    u1 = User.from_dict(user_dict)

    assert u0 == u1


def test_user_without_email():
    with pytest.raises(Exception) as excinfo:
        # missing email!
        User.from_dict({'family_name': 'Hagger'})
        print(excinfo)


def test_user_from_partial_dict():
    u0 = User.from_dict({'family_name': 'Hagger', 'email': 'hansi@somewhere.de'})
    assert u0.email is "hansi@somewhere.de"
    assert u0.onboarding is None
    assert u0.family_name is 'Hagger'


def test_with_unknown_attributes():
    with pytest.raises(Exception) as excinfo:
        User.from_dict({'email': 'junit+XCJBL6@nowhere.com', 'unknown_attribute': 'Meier XCJBL6'})
        print(excinfo)

