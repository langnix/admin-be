import json

from flask import Flask, request, make_response
from flask_restful import Resource, Api


## serializer ..
import entities.main
from utils import JSONEncoder


def output_json(data, code, headers=None):
    """Makes a Flask response with a JSON encoded body"""
    resp = make_response(json.dumps(data, cls=JSONEncoder), code)
    resp.headers.extend(headers or {})
    return resp

class MyApi(Api):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.representations = {
            'application/json': output_json,
        }

from resources import register

app = Flask(__name__)
api = MyApi(app)


register(api)
entities.main.setup_db()
if __name__ == '__main__':
    app.run(debug=True)