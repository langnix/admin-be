from flask_restful import Resource, Api


class SelfResource(Resource):
    """
    Liefert die aktuellen Berechtigungen fuer den angemeldeten User
    """
    def get(self):
        return