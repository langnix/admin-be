from .user import register as register_user


def register(api):
    register_user(api)

__all__ = ["register"]