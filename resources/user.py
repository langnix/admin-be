from flask import request
from flask_restful import Resource, abort

from entities import User
from infra import db
from services import onboarding
from services.onboarding import OnboardingStatus
from utils import id2str


class UserOnboardingResource(Resource):
    def get(self, id: str):

        return User.by_id(id).as_dict().get('onboarding')

    def delete(self, id: str):

        user = User.by_id(id)
        res = onboarding.finish(user)
        if res == OnboardingStatus.DONE:
            return None, 204
        else:
            abort(404)


class UserResource(Resource):

    def get(self, id: str):
        return id2str(User.by_id(id).as_dict())

    def patch(self, id):
        user_data = request.json

        org_user = User.by_id(id)
        new_user_dict = org_user.as_dict() | user_data
        ## TODO: wenn sich die email aendert.. neues onboarding???
        new_user: User = User.from_dict(
            new_user_dict)  # TODO: Errhandling: wenn das User-Object nicht gebaut werden kann..
        new_user.save()
        return id2str(User.by_id(id).as_dict()), 200


class UserListResource(Resource):
    def get(self):
        it = db.users.find().limit(1000)
        return [id2str(r) for r in it]

    def post(self):
        user_dict = request.get_json()
        result = db.users.insert_one(user_dict)
        user_dict['_id'] = result.inserted_id
        user = User.from_dict(user_dict)
        return id2str(onboarding.start(user).as_dict()), 201


def register(api):
    api.add_resource(UserListResource, '/users')
    api.add_resource(UserResource, '/users/<string:id>')
    api.add_resource(UserOnboardingResource, '/users/<string:id>/onboarding')
