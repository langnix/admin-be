from flask import request
from flask_restful import abort, Resource

from utils import id2str


class ClubResource(Resource):
    def get(self, id):
        res=DB.clubs.find_one({"_id": id})
        if res is None:
            abort(404, message="Club {} doesn't exist".format(id))
        return id2str(res)


class ClubListResource(Resource):
    def get(self):
        it=DB.clubs.find().limit(100)
        return [ id2str(r) for r in it]



    def post(self):
        club = request.get_json()
        result = DB.clubs.insert_one(club)
        club['_id']=result.inserted_id
        return id2str(club), 201


def register(api,db):
    api.add_resource(ClubListResource, '/clubs')
    api.add_resource(ClubResource, '/clubs/<string:id>')
    global DB
    DB=db