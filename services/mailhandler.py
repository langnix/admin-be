import smtplib

import infra
from email_templates import gen_mail_message
from entities import User
from utils import format_datetime


def send_onboarding(user: User):
    welcome_url = f"{infra.frontend_url}/#onboarding?user={user.id()}"
    expires = format_datetime(user.onboarding.expires)
    _send_user_email('onboarding', user, welcome_url=welcome_url, expires=expires)


def send_onboarding_done(user: User):
    profile_url = f"{infra.frontend_url}/profile"
    _send_user_email('onboarding-done', user, profile_url=profile_url)


def _send_user_email(template: str, user: User, **kwargs):
    with smtplib.SMTP("localhost", port=1025) as server:
        server.ehlo()  # Can be omitted
        msg = gen_mail_message(template, **kwargs)
        msg['To'] = user.email
        server.send_message(msg)
