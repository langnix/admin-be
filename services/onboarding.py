import uuid
from enum import Enum

from entities import User, Onboarding
from services.mailhandler import send_onboarding, send_onboarding_done
from utils import now


def start(user: User):
    user.onboarding = Onboarding()
    user.save()

    send_onboarding(user)
    print(f"email to: {user.email} : with id: {user.id()} start onboarding expires: {user.onboarding.expires}")
    return user


class OnboardingStatus(Enum):
    DONE = 1
    NOT_EXISTS = 2
    FINISHED = 3
    EXPIRED = 4


def finish(user: User) -> OnboardingStatus:
    if user.onboarding is None: return OnboardingStatus.NOT_EXISTS

    _ob = user.onboarding;
    if _ob.finished: return OnboardingStatus.FINISHED
    _now = now()
    if _now >= _ob.expires: return OnboardingStatus.EXPIRED

    _ob.finished = _now
    ## TODO: sub aus vom angemeldeten user holen
    user.sub = str(uuid.uuid4())

    user.save()
    send_onboarding_done(user)
    print(f"email to: {user.email} : onboarding sucessfull finished: {user.sub}")

    return OnboardingStatus.DONE
