from bson import CodecOptions
from pymongo import MongoClient
from pymongo.database import Database

## TODO: Via Environment-Variable
client = MongoClient("mongodb://root:example@localhost:27017/")
db: Database = client.get_database("billard", codec_options=CodecOptions(tz_aware=True))

frontend_url = "http://localhost:8000/#"

