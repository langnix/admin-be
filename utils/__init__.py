import json
import uuid
from dataclasses import fields
from datetime import datetime, timezone

from pytz import timezone as pytc_timezone


def id2str(dd):
    dd['_id'] = str(dd['_id'])
    return dd


def format_datetime(dt: datetime) -> str:
    ## TODO: Zeitstempel netter formatieren
    return str(dt.astimezone(pytc_timezone('Europe/Berlin')))


def now() -> datetime:
    res = datetime.now(timezone.utc)
    return res.replace(microsecond=res.microsecond - res.microsecond % 1000)  ## only milli in mongoDB...


class JSONEncoder(json.JSONEncoder):
    """Subclass of the default encoder to support custom objects"""

    def default(self, o):
        if hasattr(o, "_to_serialize"):
            # build up the object
            data = {}
            for attr in o._to_serialize:
                data[attr] = getattr(o, attr)
            return data
        elif isinstance(o, datetime):
            return o.isoformat()
        elif isinstance(o, uuid.UUID):
            return str(o)
        else:
            return json.JSONEncoder.default(self, o)

## see: https://stackoverflow.com/questions/53376099/python-dataclass-from-a-nested-dict
def dataclass_from_dict(klass, d:dict):
    ## TODO: refactor: wenn "klass kein dict oder keine dataclass ist" -> return d (dann ist d naemlich auch kein dict ..)
    try:
        fieldtypes = {f.name:f.type for f in fields(klass)}
        return klass(**{f:dataclass_from_dict(fieldtypes[f],d[f]) for f in d})
    except Exception as e:

        return d # Not a dataclass field