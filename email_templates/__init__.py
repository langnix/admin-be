import importlib.resources as pkg_resources
from dataclasses import dataclass
from email.message import EmailMessage

import yaml
from jinja2 import Environment, PackageLoader, select_autoescape

from . import templates


@dataclass
class MailDefinition:
    ident: str
    subject: str
    template: str
    sender: str


def _load_templates():
    with pkg_resources.open_binary(templates, 'index.yaml') as file:
        data = yaml.safe_load(file)
    # add ident ...
    return {k: MailDefinition(ident=k, **v) for k, v in data.items()}


_templates: dict = _load_templates()


def _get_mail_definition(template: str) -> MailDefinition:
    return _templates.get(template)


def gen_mail_message(template: str, **kwargs) -> EmailMessage:
    md = _get_mail_definition(template)
    env = Environment(
        loader=PackageLoader(__name__, 'templates'),
        autoescape=select_autoescape(['html', 'xml'])
    )
    template = env.get_template(md.template)
    txt = template.render(**kwargs)
    msg = EmailMessage()
    ## todo: subject soll auch als jinja2-template gerendert werden
    msg['Subject'] = md.subject
    msg['From'] = md.sender
    msg.set_content(txt)
    return msg
